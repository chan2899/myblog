var express = require('express');
var router = express.Router();

var User = require('../models/register');
let { encryptPassword, comparePassword, generateJwt, mailFunc  } = require('../utils/utils');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

/////////////////////////////////////register

router.post('/register', async (req, res) => {
  try {
    const emailChk = await User.findOne({ email: req.body.email }).exec();
    if (emailChk)
      throw new Error('Email is already Registered');

    let hashedPassword = await encryptPassword(req.body.password);
    req.body.password = hashedPassword;

    const newRegister = await new User(req.body).save();
await mailFunc(req.body.email);
    if (newRegister)
      res.json({ message: "Registered Successfully", success: true })
    else
      throw new Error('an err occured during registering')
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }

})



///////login


router.post('/login', async (req, res) => {
  try {
    const user = await  User.findOne({ email: req.body.email}).exec();
    if (user) {
      const passwordChk = await comparePassword(req.body.password, user.password)
      if (passwordChk) {
        const token = await generateJwt(user._id);
        res.json({ message: "successfully Logged in", data: token, success: true });
      }
      else {
        throw new Error('Check your credentials');
      }
    }
    else {
      throw new Error('Check your credentials');
    }
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }
})


router.get('/getUser', async (req, res) => {
  try {
    const user = await User.find().exec()
    if(user){
      res.json({message:"user data", data:user, success:true})
    }
    else{
      throw new Error;
    }
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }
})


router.delete('/deleteUser/:id', async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id).exec()
    if(user){
      res.json({message:"user deleted", data:user, success:true})
    }
    else{
      throw new Error;
    }
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }
})


router.patch('/updateUser/:id', async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(req.params.id,req.body).exec()
    if(user){
      res.json({message:"user successfully updated", data:user, success:true})
    }
    else{
      throw new Error;
    }
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }
})


router.get('/getSpecificUser/:id', async (req, res) => {
  try {
    const user = await User.findById(req.params.id).exec()
    if(user){
      res.json({message:"user data", data:user, success:true})
    }
    else{
      throw new Error;
    }
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }
})
module.exports = router;

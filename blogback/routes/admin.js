var express = require('express');
var router = express.Router();

var User = require('../models/admin');
let { encryptPassword, comparePassword, generateJwt } = require('../utils/utils');
const admin = require('../models/admin');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

/////////////////////////////////////register

router.post('/admin-register', async (req, res) => {
  try {
    const emailChk = await admin.findOne({ email: req.body.email }).exec();
    if (emailChk)
      throw new Error('Email is already Registered');

    let hashedPassword = await encryptPassword(req.body.password);
    req.body.password = hashedPassword;

    const newRegister = await new admin(req.body).save();

    if (newRegister)
      res.json({ message: "Registered Successfully", success: true })
    else
      throw new Error('an err occured during registering')
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }

})



///////login


router.post('/admin-login', async (req, res) => {
  try {
    const user = await  admin.findOne({ email: req.body.email}).exec();
    if (user) {
      const passwordChk = await comparePassword(req.body.password, user.password)
      if (passwordChk) {
        const token = await generateJwt(user._id);
        res.json({ message: "successfully Logged in", data: token, success: true });
      }
      else {
        throw new Error('Check your credentials');
      }
    }
    else {
      throw new Error('Check your credentials');
    }
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }
})

module.exports = router;

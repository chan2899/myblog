import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { HeaderComponent } from './index/header/header.component';
import { HomeComponent } from './index/home/home.component';
import { FooterComponent } from './index/footer/footer.component';
import { AboutComponent } from './index/about/about.component';
import { BlogComponent } from './index/blog/blog.component';
import { RegisterComponent } from './index/register/register.component';
import { LoginComponent } from './index/login/login.component';
import { BlogPostComponent } from './blogpost/blogpost.component';
import { AdminComponent } from './admindashboard/admin/admin.component';
import { AdminLoginComponent } from './admindashboard/admin-login/admin-login.component';
import { AdminRegisterComponent } from './admindashboard/admin-register/admin-register.component';
import { UserdashboardComponent } from './user/userdashboard/userdashboard.component';
import { UserheaderComponent } from './user/userheader/userheader.component';
import { UserLoginGuard } from './index/service/login/user-login.guard';
import { AdminguardGuard } from './adminguard.guard';

import { AdminAddCategoryComponent } from './admin-add-category/admin-add-category.component';

import { AdminAddSubCategoryComponent } from './admin-add-sub-category/admin-add-sub-category.component';



const routes: Routes = [
{path:'home', component:HomeComponent},
{path:'header', component:HeaderComponent},
{path:'footer', component:FooterComponent},
{path:'about',component:AboutComponent},
{path:'blog', component:BlogComponent},
{path:'register', component:RegisterComponent},
{path:'login', component:LoginComponent, canActivate: [UserLoginGuard] },
{path:'blogpost', component:BlogPostComponent},
{path:'admin', component:AdminComponent, canActivate: [AdminguardGuard]},
{path:'admin-register', component:AdminRegisterComponent, canActivate: [AdminguardGuard]},
{path:'admin-login', component:AdminLoginComponent},
{path: 'userdashboard', component:UserdashboardComponent},
{path: 'userheader', component:UserheaderComponent},
{path: 'adminaddsubcategory', component:AdminAddSubCategoryComponent},
{path: 'adminaddcategory', component:AdminAddCategoryComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

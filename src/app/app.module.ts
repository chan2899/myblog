import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './index/header/header.component';
import { HomeComponent } from './index/home/home.component';
import { FooterComponent } from './index/footer/footer.component';
import { AboutComponent } from './index/about/about.component';

import { BlogComponent } from './index/blog/blog.component';
import { LoginComponent } from './index/login/login.component';
import { RegisterComponent } from './index/register/register.component';
import { BlogPostComponent } from './blogpost/blogpost.component';
import { AdminComponent } from './admindashboard/admin/admin.component';
import { AdminLoginComponent } from './admindashboard/admin-login/admin-login.component';
import { AdminRegisterComponent } from './admindashboard/admin-register/admin-register.component';
import { UserdashboardComponent } from './user/userdashboard/userdashboard.component';
import { UserheaderComponent } from './user/userheader/userheader.component';
import { AdminAddCategoryComponent } from './admin-add-category/admin-add-category.component';

import { AdminAddSubCategoryComponent } from './admin-add-sub-category/admin-add-sub-category.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    AboutComponent,
    
    BlogComponent,
    LoginComponent,
    RegisterComponent,
    BlogPostComponent,
    AdminComponent,
    AdminLoginComponent,
    AdminRegisterComponent,
    UserdashboardComponent,
    UserheaderComponent,
    AdminAddCategoryComponent,
  
    AdminAddSubCategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  AngularEditorModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

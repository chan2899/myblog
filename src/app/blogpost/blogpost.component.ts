import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CategoryService } from '../service/category/category.service';

import { BlogpostService } from '../service/blogpost/blogpost.service'
import { SubcategoryService } from '../service/subcategory/subcategory.service';

@Component({
  selector: 'app-blogpost',
  templateUrl: './blogpost.component.html',
  styleUrls: ['./blogpost.component.css']
})
export class BlogPostComponent implements OnInit {

  constructor(private fb: FormBuilder, private category: CategoryService, private subcategory: SubcategoryService, private blog: BlogpostService) { }

  blogForm: FormGroup;
  allCategoryObj;
  allSubCategoryObj;
  tempAllSubCategoryObj;
  tempImageArr: any = [];
  displayArr: any = [];
  ngOnInit(): void {
    this.blogForm = this.fb.group({
      title: [''],
      categoryId: [''],
      subCategoryId: [''],
      blogText: [''],
      imageArr: [],
    })
    this.getCategorys();
    this.getSubCategorys();

    // console.log(this.blogForm.value)
  }

  async getCategorys() {
    const res: any = await this.category.getCategorys();
    this.allCategoryObj = res.data;
  }

  async addBlog() {
    const res: any = await this.blog.addBlog(this.blogForm.value);
    if (res.success) {
      alert(res.message)
    }
    else {
      alert(res.message)
    }
    window.location.reload();
  }

  async getSubCategorys() {
    const res: any = await this.subcategory.getSubCategorys();
    this.allSubCategoryObj = res.data;
    this.tempAllSubCategoryObj = res.data;

  }


  uploadImage(event) {

    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.tempImageArr.push(reader.result);
        this.displayArr.push(event.target.value.split(/(\\|\/)/g).pop());
        this.blogForm.patchValue({
          imageArr: this.tempImageArr
        })
        // console.log(this.specificationForm.value);
      }
    }

  }
  //////////////////////////////deleting  image from array
  deleteImage(i) {
    this.tempImageArr.splice(i, 1)
    this.displayArr.splice(i, 1)
    this.blogForm.patchValue({
      imageArr: this.tempImageArr
    })
  }

  /////////////////////filter logic starts here 

  filterSubCategory(event) {
    let tempId = event.target.value;
    this.tempAllSubCategoryObj = this.allSubCategoryObj.filter(el => el.categoryId == tempId)
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '500px',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/Image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
    ]
  };
}

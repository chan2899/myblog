import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms'
import { from } from 'rxjs';
import { Router } from '@angular/router';

import { AdminregisterService } from '../service/adminregister/adminregister.service';

@Component({
  selector: 'app-register',
  templateUrl: './admin-register.component.html',
  styleUrls: ['./admin-register.component.css']
})
export class AdminRegisterComponent implements OnInit {

  registerForm:FormGroup
  constructor(private fb:FormBuilder, private register:AdminregisterService, private router:Router) { }

  ngOnInit(): void {

    this.registerForm=this.fb.group({
      email:[''],
      firstname:[''],
    lastname:[''],
      password:[''],
      username:['']
      
      
    })

  }
  
  async Register(){
    console.log(this.registerForm)
    const res:any = await this.register.Register(this.registerForm.value)
    if(res.success){
      alert(res.message)
    }
    else
    alert(res.message)
   
   
    this.router.navigateByUrl('/admin-login')
  
  }

}
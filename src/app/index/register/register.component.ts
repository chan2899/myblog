import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms'
import { from } from 'rxjs';
import { RegisterService } from '../service/register/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm:FormGroup
  constructor(private fb:FormBuilder, private register:RegisterService) { }

  ngOnInit(): void {

    this.registerForm=this.fb.group({
      email:[''],
      firstname:[''],
    lastname:[''],
      password:[''],
      username:['']
      
      
    })

  }

  async Register(){
    console.log(this.registerForm)
    const res:any = await this.register.Register(this.registerForm.value)
    if(res.success){
      alert(res.message)
    }
    else
    alert(res.message)
  }

}
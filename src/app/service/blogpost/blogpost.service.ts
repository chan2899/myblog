import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlogpostService {
  url = "http://localhost:3000"

  constructor(private http: HttpClient) { }
  async addBlog(formData) {
    return this.http.post(`${this.url}/blog/addblog`, formData).toPromise()
  }

  async getBlogs() {
    return this.http.get(`${this.url}/blog/getAllblogs`).toPromise();
  }

  async getParticularBlogs(id) {
    return this.http.get(`${this.url}/blog/getSpecificblog/${id}`).toPromise()
  }
}
